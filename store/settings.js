// eslint-disable-next-line no-unused-vars
import consola from 'consola'

// import { stateAbbreviations } from '~/data/lookups'

export const state = () => ({
  yScale: 'total',
  measuremantName: 'cases'
})

export const getters = {
  pageTitle: (state) => state.pageTitle,
  nationName: (state) => state.nationName,
  stateName: (state) => state.stateName,
  countyName: (state) => state.countyName,
  yScale: (state) => state.yScale,
  measuremantName: (state) => state.measuremantName
}

export const mutations = {
  pageTitle(state, value) {
    state.pageTitle = value
  },
  nationName(state, value) {
    state.nationName = value
  },
  stateName(state, value) {
    state.stateName = value
  },
  countyName(state, value) {
    state.countyName = value
  },
  yScale(state, value) {
    state.yScale = value
  },
  measuremantName(state, value) {
    state.measuremantName = value
  }
}

export const actions = {
  pageTitle({ commit }, value) {
    commit('pageTitle', value)
  },
  nationName({ commit }, value) {
    commit('nationName', value)
  },
  stateName({ commit }, value) {
    commit('stateName', value)
  },
  countyName({ commit }, value) {
    commit('countyName', value)
  },
  yScale({ commit }, value) {
    commit('yScale', value)
  },
  measuremantName({ commit }, value) {
    commit('measuremantName', value)
  }
}

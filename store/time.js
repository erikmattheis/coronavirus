// eslint-disable-next-line no-unused-vars
import consola from 'consola'
import world from '~/data/data.json'
// import { stateAbbreviations } from '~/data/lookups'

export const state = () => ({})

export const getters = {
  timeSeries: (state) => (subject, nationName, stateName, countyName) => {
    let filter
    consola.log('finding series for ', subject, nationName, stateName)
    if (countyName) {
      consola.log('looking for county')
      filter = (el) =>
        el.nationName === nationName &&
        el.stateName === stateName &&
        el.countyName === countyName
    } else if (stateName) {
      consola.log('looking for state')
      filter = (el) =>
        el.nationName === nationName &&
        el.stateName === stateName &&
        !el.countyName
    } else if (nationName) {
      consola.log('looking for country')
      filter = (el) => el.nationName === nationName && !el.stateName
    } else {
      filter = (el) => el.nationName === 'United States' && !el.stateName
    }
    const result = Array.from(world.regions).find(filter)

    const series = {
      data: result.casesHistory,
      dates: world.dates
    }

    return series
  },

  newCases: (state) => (subject, nationName, stateName, countyName) => {
    let filter
    consola.log('finding series for ', subject, nationName, stateName)
    if (countyName) {
      consola.log('looking for county')
      filter = (el) =>
        el.nationName === nationName &&
        el.stateName === stateName &&
        el.countyName === countyName
    } else if (stateName) {
      consola.log('looking for state')
      filter = (el) =>
        el.nationName === nationName &&
        el.stateName === stateName &&
        !el.countyName
    } else if (nationName) {
      consola.log('looking for country')
      filter = (el) => el.nationName === nationName && !el.stateName
    } else {
      filter = (el) => el.nationName === 'United States' && !el.stateName
    }
    const result = Array.from(world.regions).find(filter)

    let lastValue = 0
    result.deathsHistory.forEach(function(o, i, a) {
      const tempValue = o - lastValue
      lastValue = o
      a[i] = tempValue
    })
    const series = {
      data: result.deathsHistory,
      dates: world.dates
    }

    return series
  },

  total: (state, getters) => (
    casesOrDeaths,
    nationName,
    stateName,
    minPop,
    allUSCounties
  ) => {
    const regions = getters.regions(
      nationName,
      stateName,
      minPop,
      allUSCounties
    )

    regions.sort((a, b) => a[casesOrDeaths] - b[casesOrDeaths])
    regions.forEach((el) => (el.value = el[casesOrDeaths]))

    return regions
  },
  percentage: (state, getters) => (
    casesOrDeaths,
    nationName,
    stateName,
    minPop,
    allUSCounties
  ) => {
    const regions = getters.regions(
      nationName,
      stateName,
      minPop,
      allUSCounties
    )

    regions.forEach((el) => {
      el.value = +((el[casesOrDeaths] * 100) / el.population).toFixed(2)
    })

    regions.sort((a, b) => a.value - b.value)

    return regions
  },
  wow: (state, getters) => (
    casesOrDeaths,
    nationName,
    stateName,
    minPop,
    allUSCounties
  ) => {
    const regions = getters.regions(
      nationName,
      stateName,
      minPop,
      allUSCounties
    )
    casesOrDeaths += 'History'
    regions.forEach(function(o, i, a) {
      const newCasesLastWeek =
        o[casesOrDeaths][o[casesOrDeaths].length - 8] -
        o[casesOrDeaths][o[casesOrDeaths].length - 14]
      const newCasesThisWeek =
        o[casesOrDeaths][o[casesOrDeaths].length - 1] -
        o[casesOrDeaths][o[casesOrDeaths].length - 7]
      if (newCasesLastWeek === 0) {
        consola.log('what to do?', newCasesThisWeek, '/', newCasesLastWeek)
        a[i].value = 100
      } else {
        a[i].value = +(100 * (newCasesThisWeek / newCasesLastWeek - 1)).toFixed(
          2
        )
      }
    })

    regions.sort((a, b) => a.value - b.value)

    return regions
  }
}

export const mutations = {}

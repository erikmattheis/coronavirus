// eslint-disable-next-line no-unused-vars
import consola from 'consola'
import world from '~/data/data.json'
// import { stateAbbreviations } from '~/data/lookups'

export const state = () => ({})

export const getters = {
  regions: (state) => (nationName, stateName, minPop, allUSCounties) => {
    let filter
    consola.log('state.regions', nationName, stateName, minPop, allUSCounties)
    if (allUSCounties) {
      filter = (el) => el.nationName === 'United States' && el.countyName
    } else {
      filter = regionsFilter(nationName, stateName)
    }

    const regions = Array.from(world.regions)
      .filter(filter)
      .filter((el) => !minPop || el.population >= minPop)

    if (allUSCounties) {
      regions.forEach(function(el) {
        el.regionName = el.countyName + ', ' + el.stateName
      })
    }
    return regions
  },

  total: (state, getters) => (
    casesOrDeaths,
    nationName,
    stateName,
    minPop,
    allUSCounties
  ) => {
    const regions = getters.regions(
      nationName,
      stateName,
      minPop,
      allUSCounties
    )

    regions.sort((a, b) => a[casesOrDeaths] - b[casesOrDeaths])
    regions.forEach((el) => (el.value = el[casesOrDeaths]))

    return regions
  },
  percentage: (state, getters) => (
    casesOrDeaths,
    nationName,
    stateName,
    minPop,
    allUSCounties
  ) => {
    const regions = getters.regions(
      nationName,
      stateName,
      minPop,
      allUSCounties
    )

    regions.forEach((el) => {
      el.value = +((el[casesOrDeaths] * 100) / el.population).toFixed(2)
    })

    regions.sort((a, b) => a.value - b.value)

    return regions
  },
  wow: (state, getters) => (
    casesOrDeaths,
    nationName,
    stateName,
    minPop,
    allUSCounties
  ) => {
    const regions = getters.regions(
      nationName,
      stateName,
      minPop,
      allUSCounties
    )
    casesOrDeaths += 'History'
    regions.forEach(function(o, i, a) {
      const newCasesLastWeek =
        o[casesOrDeaths][o[casesOrDeaths].length - 8] -
        o[casesOrDeaths][o[casesOrDeaths].length - 14]
      const newCasesThisWeek =
        o[casesOrDeaths][o[casesOrDeaths].length - 1] -
        o[casesOrDeaths][o[casesOrDeaths].length - 7]
      if (newCasesLastWeek === 0) {
        consola.log('what to do?', newCasesThisWeek, '/', newCasesLastWeek)
        a[i].value = 100
      } else {
        a[i].value = +(100 * (newCasesThisWeek / newCasesLastWeek - 1)).toFixed(
          2
        )
      }
    })

    regions.sort((a, b) => a.value - b.value)

    return regions
  }
}

export const mutations = {}

function regionsFilter(nationName, stateName) {
  if (nationName) {
    if (stateName) {
      return function(el) {
        return (
          el.nationName === nationName &&
          el.stateName === stateName &&
          el.countyName
        )
      }
    } else {
      return function(el) {
        return el.nationName === nationName && el.stateName && !el.countyName
      }
    }
  }
  return function(el) {
    return !el.stateName
  }
}

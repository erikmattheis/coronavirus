// eslint-disable-next-line no-unused-vars
import consola from 'consola'
import world from '~/data/data.json'
// import { stateAbbreviations } from '~/data/lookups'

export const state = () => ({})

export const getters = {
  europeanUnion: () => {
    return [
      'Austria',
      'Belgium',
      'Bulgaria',
      'Croatia',
      'Republic of Cyprus',
      'Czech Republic',
      'Denmark',
      'Estonia',
      'Finland',
      'France',
      'Germany',
      'Greece',
      'Hungary',
      'Ireland',
      'Italy',
      'Latvia',
      'Lithuania',
      'Luxembourg',
      'Malta',
      'Netherlands',
      'Poland',
      'Portugal',
      'Romania',
      'Slovakia',
      'Slovenia',
      'Spain',
      'Sweden'
    ]
  },
  g8: () => {
    return [
      ...[
        'Canada',
        'France',
        'Germany',
        'Italy',
        'Japan',
        'Russia',
        'United Kingdom',
        'United States'
      ],
      ...this.europeanUnion
    ]
  },

  getRegions: (state) => {
    world.regions
      .filter((region) => !region.stateName)
      .map((el) => el.nationName)
  },

  getRegionNames: (state) => (levelName) => {
    let regions
    if (levelName === 'world') {
      regions = world.regions
        .filter((el) => el.nationName && !el.stateName)
        .map((el) => {
          return el.nationName
        })
        .sort()
    } else if (levelName === 'nationName') {
      regions = world.regions
        .filter((el) => el.stateName && !el.countyName)
        .map((el) => el.stateName)
        .sort()
    } else if (levelName === 'state') {
      regions = world.regions
        .filter((el) => el.countyName)
        .map((el) => el.countyName)
        .sort()
    }
    return regions
  }
}

// eslint-disable-next-line no-unused-vars
import consola from 'consola'
// import world from '~/data/data.json'
// import { stateAbbreviations } from '~/data/lookups'
/*
export const state = () => ({})

export const getters = {
  getOptions: (state) => (regions, regionNames, yOptions) => {
    const labelSuffix = yOptions === 'percentge' ? '%' : null
    const result = {
      chart: chart(),

      plotOptions: plotOptions(yOptions),
      xaxis: xaxis(regionNames),

      colors: regions.map((el) => (el.hasSubregions ? '#007bff' : '#4D6277')),

      yaxis: yaxis(labelSuffix),
      legend: {
        show: false
      },
      toolbar: {
        show: false
      }
    }
    return result
  }
}

function xaxis(regionNames) {
  return {
    // type: 'category',
    labels: {
      style: {
        colors: '#bcc6cc',
        fontSize: '14px',
        fontFamily: 'Arial, sans-serif'
      }
    },
    // position: 'bottom',
    categories: regionNames
  }
}

function yaxis(labelSuffix) {
  return {
    floating: false,
    labels: {
      minWidth: 30,
      maxWidth: 30,
      formatter(val) {
        if (labelSuffix === '%') {
          return val.toFixed(2) + '%'
        } else if (Number(val) < 1000) {
          return val
        } else if (Number(val) < 1000000) {
          return Math.round(val / 10000) + 'K'
        }
        return Math.round(val / 1000000) + 'M'
      }
    }
  }
}

function chart() {
  return {
    foreColor: '#999',
    type: 'bar',
    events: {
      dataPointSelection(event, chartContext, config) {
        if (this.levelName === 'countyName') {
          return
        }
        this.selectRegion(
          config.w.config.xaxis.categories[config.dataPointIndex]
        )
      },

      updated() {
        this.dataLoading = false
      }
    }
  }
}

function plotOptions(yOptions) {
  return {
    bar: {
      horizontal: false,
      // endingShape: 'flat',
      columnWidth: '86%',
      // barHeight: '70%',
      distributed: true
      
      //colors: {
      //  ranges: [
      //    {
      //      from: 0,
      //      to: 0,
      //      color: undefined
      //    }
      //  ],
      //  backgroundBarColors: [],
      //  backgroundBarOpacity: 1
      //},
      
      // dataLabels: dataLabels(yOptions)
    }
  }
}

function dataLabels(yOptions) {
  return {
    color: '#ddd',
    formatter(val, opts) {
      if (yOptions === 'percentage') {
        return val + '%'
      }
      return val < 1000 ? val : (Math.round(val * 10) / 10000).toFixed(1) + 'K'
    },
    offsetY: -18
  }
}

export function getTimeSeriesOptions(seriesLength) {
  return {
    chart: {
      type: 'bar'
    },
    dataLabels: {
      enabled: true
    },
    stroke: {
      curve: 'straight'
    },
    xaxis: {
      categories: this.$store.getters.datesTimeSeries(seriesLength)
    }
  }
}
*/

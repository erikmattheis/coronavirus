import data from './data/data.json'

export default {
  mode: 'universal',
  server: {
    port: 3000, // default: 3000
    host: 'localhost' // default: localhost
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [{ src: '~plugins/ga.js', mode: 'client' }],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    'nuxt-vuex-router-sync'
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  },
  router: {
    /*
    extendRoutes(routes, resolve) {
      routes.push({
        path: '/:nation',
        components: {
          default: resolve(__dirname, 'pages/users')
        },
        chunkNames: {
          modal: 'components/modal'
        }
      })
    }
    */
  }
  /*
  generate: {
    routes: dynamicRoutes
  }
  */
}

// eslint-disable-next-line no-unused-vars
function dynamicRoutes() {
  /*
  const worldTree = {}
const unique
  data.regions.forEach(function(el) {
    if (!uniqueNations.includes(el.nationName)) {
      uniqueNations.push(el.nationName)
      worldTree[el.nationName] = {}
    }
  })
  worldTree.forEach(function(el0) {
    uniqueStates = world.map(
      (el1) => el0.nationName === el1.nationName && el1.stateName && !el1.countyName
    )
    uniqueStates.forEach(function(el) {
      el0[el0.nationName][uniqueStates.stateName] = {}
    })
  })
  worldTree.forEach(function(el0) {
    uniqueCounties = world.map(
      (el1) =>
        el0.nationName === el1.nationName &&
        el1.stateName == el1.stateName &&
        el1.countyName
    )
    uniqueCounties.forEach(function(el) {
      el0[el0.nationName][el0.stateName][el1.countyName] = {}
    })
  })
  const flatWorld = []`
  */
  const flatWorld = []

  data.regions.forEach(function(el, i) {
    let pathValue = `/${el.nationName}`
    if (el.stateName) {
      pathValue += `/${el.stateName}`
    }
    if (el.countyName) {
      pathValue += `/${el.countyName}`
    }

    flatWorld.push(pathValue)
  })

  return new Promise((resolve) => {
    resolve(flatWorld)
  })
}

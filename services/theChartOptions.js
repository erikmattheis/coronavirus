import consola from 'consola'

// eslint-disable-next-line no-unused-vars
export default getTop10NationsOptions(seriesLength) {
  return {
    chart: {
      events: {
        dataPointSelection(event, chartContext, config) {
          consola.info(chartContext, config)
        },

        updated() {
          this.dataLoading = false
        }
      }
    },
    plotOptions: {
      bar: {
        horizontal: false,
        endingShape: 'flat',
        columnWidth: '86%',
        barHeight: '70%',
        distributed: false,
        colors: {
          ranges: [
            {
              from: 0,
              to: 0,
              color: undefined
            }
          ],
          backgroundBarColors: [],
          backgroundBarOpacity: 1
        },
        dataLabels: {
          position: 'top',
          maxItems: 100,
          hideOverflowingLabels: true,
          orientation: 'horizontal'
        }
      }
    },
    title: {
      text: `Updated .`,
      style: {
        fontSize: '16px',
        fontWeight: 'bold',
        fontFamily: 'Arial, sans-serif',
        color: '#bcc6cc'
      }
    },
    subtitle: {
      text: 'Data from John Hopkins Whiting School of Engineering.',
      offsetY: 20,
      style: {
        fontSize: '14px',
        fontWeight: 'normal',
        fontFamily: 'Arial, sans-serif',
        color: '#bcc6cc'
      }
    },
    xaxis: {
      type: 'category',
      labels: {
        style: {
          colors: '#bcc6cc',
          fontSize: '14px',
          fontFamily: 'Arial, sans-serif'
        }
      },
      axisBorder: {
        color: '#78909C'
      },
      axisTicks: {
        color: '#78909C'
      },
      position: 'bottom'
    },
    yaxis: {
      logarithmic: false,
      labels: {
        style: {
          colors: '#bcc6cc',
          fontSize: '14px',
          fontFamily: 'Arial, sans-serif'
        },
        formatter(value) {
          value = value < 1000 ? value : `${Math.ceil(value / 1000)}K`
          return value
        }
      },
      axisBorder: {
        color: '#bcc6cc'
      },
      axisTicks: {
        color: '#bcc6cc'
      }
    }
  }
}
/*
export function getTimeSeriesOptions(seriesLength) {
  return {
    chart: {
      type: 'bar'
    },
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: 'straight'
    },
    xaxis: {
      categories: this.$store.getters.datesTimeSeries(seriesLength)
    }
  }
}
*/